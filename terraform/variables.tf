variable "credentials" {
  description = "GCP_KEY"
  type = string
}

variable "ssh_keys" {
  description = "Chave para conexão ssh"
  type = string
}

variable "project" {
  description = "Project-ID"
  type = string
}

variable "region" {
  description = "Região GCP"
  type = string
}

variable "zone" {
  description = "Região da Instancia"
  type = string
}

variable "name_vpc_network" {
  description = "Nome da VPC"
  type = string
}

variable "name_subnet" {
  description = "Nome da Sub-net"
  type = string
}

variable "ip_cidr_range" {
  description = "Região da Instancia"
  type = string
}

variable "name_firewall" {
  description = "Região da Instancia"
  type = string
}

variable "name_instance" {
  description = "Região da Instancia"
  type = string
}

variable "tipo_maquina" {
  description = "Região da Instancia"
  type = string
}


