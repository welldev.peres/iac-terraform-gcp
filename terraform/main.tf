provider "google" {
  credentials = file(var.credentials)
  project     = var.project
  region      = var.region

}

resource "google_compute_network" "vpc_network" {
  name                    = var.name_vpc_network
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "subnet" {
  name          = var.name_subnet
  ip_cidr_range = var.ip_cidr_range
  region        = var.region
  network       = google_compute_network.vpc_network.name
}

resource "google_compute_firewall" "default" {
  name    = var.name_firewall
  network = google_compute_network.vpc_network.name

  allow {
    protocol = "tcp"
    ports    = ["80", "443", "22", "8000"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_instance" "vm_instance" {
  name         = var.name_instance
  machine_type = var.tipo_maquina
  zone         = var.zone

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2204-lts"
    }
  }

  network_interface {
    network    = google_compute_network.vpc_network.name
    subnetwork = google_compute_subnetwork.subnet.name

    access_config {
      //34.151.220.126
    }
  }

  metadata = {
    ssh-keys = "welldev.peres:${var.ssh_keys}"
  }

  metadata_startup_script = <<-EOT
    #! /bin/bash
    sudo apt-get update
    sudo apt-get install -y nginx
  EOT
}